import { Application, Loader } from 'pixi.js'
import Factory from './Factory/Factory';

const appWidth = window.innerWidth;
const appHeight = window.innerHeight;
export const app = new Application({
  width: appWidth,
  height: appHeight,
  resolution: window.devicePixelRatio,
  backgroundColor: 0x22A7F0
})
export function loader() {
  Loader.shared
.add([
  "assets/cloud_1.png",
  "assets/cloud_2.png",
  "assets/spaceship.png",
  "assets/playerShot.png",
  "assets/enemies.png",
  "assets/enemyShot.png",
  "assets/explosion.png",
]).load(init);
}

let scoreBar = Factory.createScoreBar();
let cloudManager;
let player;
let enemies;
let destroy;
let stage;
export function init()
{
  scoreBar.init();
  stage = Factory.createStage();
  cloudManager = Factory.createCloudManager(stage, appWidth, appHeight);
  enemies = Factory.createEnemies(stage, appWidth, appHeight, scoreBar);
  player = Factory.createPlayer(stage, appWidth, appHeight);
  destroy = Factory.createDestroy(player, enemies, scoreBar)

  app.renderer.render(stage);
  loop();
}

export function reset() {
  stage = Factory.createStage();
  scoreBar.reset()
  cloudManager = Factory.createCloudManager(stage, appWidth, appHeight);
  enemies = Factory.createEnemies(stage, appWidth, appHeight, scoreBar);
  player = Factory.createPlayer(stage, appWidth, appHeight);
  destroy = Factory.createDestroy(player, enemies, scoreBar)

  app.renderer.render(stage);
}
function loop()
{
  cloudManager.update();
  destroy.playerDie();
  destroy.enemyDie();
  enemies.update();
  player.update();

  requestAnimationFrame(loop);

  app.renderer.render(stage);
}

loader();
document.body.appendChild(app.view);
