import { Loader, Sprite } from "pixi.js";
import FiringManager from "../Managers/FiringManager";

export default class SingleEnemy extends Sprite {
  constructor(stage, png, appWidth, appHeight) {
    super(Loader.shared.resources[`assets/${png}.png`].texture);
    this.appWidth = appWidth;
    this.appHeight = appHeight;
    this.stage = stage;
    this.velocity = { x: 0, y: 0 };
    this.create();
    this.speed = 4;
    this.shootManager = new FiringManager("enemyShot", this.appWidth, this.appHeight);
    this.shootManager.setSpeed(8);
    this.canFire = true;
    this.fireDelay = 1500;
  }

  getShots() {
    return this.shootManager.list;
  }

  create() {
    this.anchor.set(0.5, 0.5);
    this.position.set(this.setRandomX(), this.setRandomY());
    this.scale.set(1, 1);
    this.stage.addChild(this);
  }

  setRandomX() {
    return Math.ceil(this.appWidth + this.appWidth * Math.random());
  }

  setRandomY() {
    return Math.ceil(this.appHeight * Math.random());
  }
  updateFiring() {
    if (this.canFire) {
      this.shootManager.createShot(
        this.stage,
        {
          x: this.position.x - this.width / 4,
          y: this.position.y,
        },
        2,
        2
      );

      this.resetFireDelay();
    }
  }
  resetFireDelay() {
    this.canFire = false;

    setTimeout(() => (this.canFire = true), this.fireDelay);
  }
  update() {
    if (this.visible) {
      this.updateFiring();
    }
    this.position.x -= this.speed;
    this.shootManager.leftDestination();

  }

  reset() {
    if (this.position.x < -this.width) {
      this.position.set(
        this.appWidth + this.appWidth * Math.random(),
        this.appHeight * Math.random()
      );
    }
  }

  getSize() {
    const fillPoints = (position, size) => {
      const arr = [];
      for (
        let i = Math.ceil(position - size / 4);
        i <= Math.ceil(position + size / 4);
        i++
      ) {
        arr.push(i);
      }

      return arr;
    };

    return {
      x: fillPoints(this.position.x, this.width / 1.5),
      y: fillPoints(this.position.y, this.height / 1.5),
    };
  }
}
