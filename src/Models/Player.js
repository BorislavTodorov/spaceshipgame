import { Loader, Sprite } from 'pixi.js'
import FiringManager from '../Managers/FiringManager';

export default class Player extends Sprite
{
  constructor(stage, appWidth, appHeight) {
    super(Loader.shared.resources["assets/spaceship.png"].texture);
    this.stage = stage;
    this.appWidth = appWidth;
    this.appHeight = appHeight;
    this.anchor.set(0.5, 0.5);
    this.scale.set(1, 1);
    this.position.set(Math.trunc(this.appWidth) * 0.1, Math.trunc(this.appHeight * 0.4));
    stage.addChild(this);
    this.velocity = { x: 0, y: 0 };
    this.speed = 6;
    this.keysState = { 32: false, 37: false, 38: false, 39: false, 40: false };
    window.addEventListener('keydown', this.onKeyDown.bind(this));
    window.addEventListener('keyup', this.onKeyUp.bind(this));
    this.shootManager = new FiringManager('playerShot', this.appWidth, );
    this.canFire = true;
    this.fireDelay = 500;

  }
  getSize() {
    const fillPoints = (position, size) => {
      const arr = [];
      for (let i = Math.ceil(position - size / 2); i <= Math.ceil(position + size / 2); i++) {
        arr.push(i)
      };

      return arr
    }

    return {
      x: fillPoints(this.position.x, this.width / 2),
      y: fillPoints(this.position.y, this.height / 2),
    }
  }
  onKeyDown(key) {
    const velocities = { 37: -1, 38: -1, 39: 1, 40: 1 };

    this.keysState[key.keyCode] = true;

    if (key.keyCode == 37 || key.keyCode == 39) {
      this.velocity.x = velocities[key.keyCode];
    } else if (key.keyCode == 38 || key.keyCode == 40) {
      this.velocity.y = velocities[key.keyCode];
    }
  }

  onKeyUp(key) {
    this.keysState[key.keyCode] = false;

    if (key.keyCode == 37 || key.keyCode == 39) {
      this.velocity.x = 0;
    } else if (key.keyCode == 38 || key.keyCode == 40) {
      this.velocity.y = 0;
    }
  }
  updateFiring() {
    if (this.keysState[32] && this.canFire) {
      this.shootManager.createShot(this.stage, {
        x: this.position.x + this.width / 2,
        y: this.position.y
      }, 1, 1);
      this.resetFireDelay();
    }
  }
  resetFireDelay() {
    this.canFire = false;

    setTimeout(() => this.canFire = true, this.fireDelay);
  }
  update() {

    this.updateFiring();
    this.shootManager.rightDestination();
    let nextX = Math.trunc(this.position.x + this.velocity.x * this.speed);
    let nextY = Math.trunc(this.position.y + this.velocity.y * this.speed);
    if (nextX > 0 && nextX < this.appWidth) {
        this.position.x = nextX;
    }
    if (nextY > 0 && nextY < this.appHeight) {
        this.position.y = nextY;
    }
  }
}
