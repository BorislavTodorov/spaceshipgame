
export default class ScoreBar {
  constructor() {
    this.scoreElement = document.getElementById("score");
    this.levelElement = document.getElementById("level");
    this.bestScoreElement = document.getElementById("best-score");
    this.score = 0;
    this.level = 0;

  }

  getScore() {
    return this.score
  }
  init() {
    this.scoreElement.textContent = `Score: ${this.score}`
    this.levelElement.textContent = `Level: ${this.level}`
    this.bestScoreElement.textContent = `Best Score: ${localStorage.getItem('bestScore') | 0}`
  }
  incrementScore() {
    this.score += 10;
    this.scoreElement.textContent = `Score: ${this.score}`
  }
  saveBestScore(score) {
    const currentBestScore = Number(localStorage.getItem('bestScore')) | 0;
    if (score > currentBestScore) {
      localStorage.setItem('bestScore', String(score))
    }
  }

  incrementLevel() {
      this.level += 1;
      this.levelElement.textContent = `Level: ${this.level}`
  }

  reset() {
    this.level = 0;
    this.score = 0;
    this.scoreElement.textContent = `Score: 0`
    this.levelElement.textContent = `Level: 0`
    this.bestScoreElement.textContent = `Best Score: ${localStorage.getItem('bestScore') | 0}`
  }

}
