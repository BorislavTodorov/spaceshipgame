import Factory from "../Factory/Factory";

export default class Enemies {
  constructor(stage, appWidth, appHeight, scoreBar) {
    this.stage = stage;
    this.speed = 4;
    this.appWidth = appWidth;
    this.appHeight = appHeight;
    this.scoreBar = scoreBar;
    this.delay = 1500;
    this.list = [
      Factory.createEnemy(this.stage, "enemies", this.appWidth, this.appHeight),
    ];
    this.incrementFunction = null;
    this.incrementEnemiesNumber(true);
  }

  getEnemies() {
    return this.list;
  }

  incrementEnemiesNumber(isIncrement) {
    if (isIncrement) {
      this.incrementFunction = setInterval(() => {
        this.list.push(
          Factory.createEnemy(
            this.stage,
            "enemies",
            this.appWidth,
            this.appHeight
          )
        );
      }, this.delay);
    } else {
      clearInterval(this.incrementFunction);
    }
  }

  incrementLevel() {
    if (this.scoreBar.getScore() % 20 === 0) {
      if (this.delay > 100) {
        clearInterval(this.incrementEnemiesNumber);
        this.delay -= 100;
        this.scoreBar.incrementLevel();
        this.incrementEnemiesNumber(false);
        this.incrementEnemiesNumber(true);
      }
    }
  }

  update() {
    this.list.forEach((element) => {
      element.update();
      element.reset();
    });
  }

  remove(index) {
    this.list.splice(index, 1);
  }
}
