import { reset } from "../index";
import { Loader } from "pixi.js";

export default class Destroy {
  constructor(player, enemies, scoreBar) {
    this.player = player;
    this.enemyList = enemies.getEnemies()
    this.enemies = enemies;
    this.scoreBar = scoreBar;
  }

  playerDie() {
    const playerSize = this.player.getSize();
    if (this.enemyList.length) {
      for (let i = 0; i < this.enemyList.length; i++) {
        const enemy = this.enemyList[i];
        const enemyShots = enemy.getShots();
        if (
          enemyShots.some(
            (shot) => {
              if (
                playerSize.x.some((x) => shot.position.x === x) &&
                playerSize.y.some((y) => shot.position.y === y)
              ) {

                return true
              } else if (
                playerSize.x.some((x) => enemy.getSize().x.some((y) => y === x)) &&
                playerSize.y.some((x) => enemy.getSize().y.some((y) => y === x))
              ) {

                return true
              } else {

                return false
              }
            }
          )
        ) {
          this.scoreBar.saveBestScore(this.scoreBar.getScore())
          reset();
        }
      }
    }
  }

  enemyDie() {
    const playerShots = this.player.shootManager.list.length;
    if (playerShots > 0 && this.enemyList.length > 0) {
      for (let i = 0; i < this.enemyList.length; i++) {
        if (this.enemyList[i].visible) {
          const enemy = this.enemyList[i].getSize();
          for (let y = 0; y < playerShots; y++) {
            const shot = this.player.shootManager.list[y];
            if (shot && enemy.x.includes(shot.x) && enemy.y.includes(shot.y)) {
              const destroyedEnemy = this.enemyList[i];
              this.player.shootManager.destroyShot(shot);
              const explosion = Loader.shared.resources[`assets/explosion.png`].texture;
              destroyedEnemy.texture = explosion;
              this.scoreBar.incrementScore();
              this.enemies.incrementLevel();
              setTimeout(() => {
                destroyedEnemy.visible = false;
                const delay = () => setTimeout(() => {
                  if (destroyedEnemy.shootManager.list.length === 0) {
                    this.enemies.remove(i)
                    destroyedEnemy.destroy();
                  } else {
                    delay()
                  }
                }, 500);
              }, 100);
            }
          }
        }
      }
    }
  }
}
