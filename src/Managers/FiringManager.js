import { Loader, Sprite } from "pixi.js";
export default class FiringManager {
  constructor(img, appWidth) {
    this.list = [];
    this.speed = 6;
    this.img = img;
    this.appWidth = appWidth;
  }
  setSpeed(speed) {
    if (speed >= 6 && speed <= 30) {
      this.speed = speed;
    }
  }
  createShot(stage, position, scaleX, scaleY) {
    const shot = new Sprite(
      Loader.shared.resources[`assets/${this.img}.png`].texture
    );
    shot.anchor.set(0.5, 0.5);
    shot.scale.set(scaleX || 0.5, scaleY || 0.5);
    shot.position.set(position.x, position.y);
    this.list.push(shot);
    stage.addChild(shot);
  }
  rightDestination() {
    if (this.list.length) {
      this.list.forEach((element) => {
        element.position.x += this.speed;
        if (element.position.x > this.appWidth) {
          this.destroyShot(element)
        }
      });
    }

  }
  leftDestination() {
    if (this.list.length) {
      this.list.forEach((element) => {
        element.position.x -= this.speed;
        if (element.position.x < 0) {
          this.destroyShot(element)
        }
      });
    }

  }

  destroyShot(element) {
    this.list.splice(this.list.indexOf(element), 1);
    element.destroy();
  }
}
