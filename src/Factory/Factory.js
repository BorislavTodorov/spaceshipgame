import { Container } from 'pixi.js';
import CloudManager from '../Managers/CloudManager';
import Destroy from '../Managers/DestroyManager';
import Enemies from '../Managers/EnemiesManager';
import FiringManager from '../Managers/FiringManager';
import Player from '../Models/Player';
import ScoreBar from '../Models/ScoreBar';
import SingleEnemy from '../Models/SingleEnemy';


export default class Factory {
    static createEnemies (stage, png, appWidth, appHeight) {
        return new Enemies(stage, png, appWidth, appHeight)
    }
    static createPlayer (stage, appWidth, appHeight) {
        return new Player(stage, appWidth, appHeight)
    }
    static createFiringManager (img, appWidth) {
        return new FiringManager(img, appWidth)
    }
    static createEnemy (stage, img, appWidth, appHeight) {
        return new SingleEnemy(stage, img, appWidth, appHeight)
    }
    static createScoreBar () {
        return new ScoreBar()
    }
    static createStage () {
        return new Container()
    }
    static createDestroy (player, enemies, scoreBar) {
        return new Destroy(player, enemies, scoreBar)
    }
    static createCloudManager (stage, appWidth, appHeight) {
        return new CloudManager(stage, appWidth, appHeight)
    }


}
