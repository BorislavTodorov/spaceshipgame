# Spaceships

Game with spaceships.
Use the arrow keys to move the character and shoot with the spacebar.
If he is hit by an opponent's shot or collides with an opponent, he dies, his best result is preserved and the game is restarted.

## Installation

```bash
npm install
```

## Usage

```bash
npm start
```

## Author
B.Todorov
